#!/usr/bin/env sh

set -eu

createFilesSubdirectories() {
  process='Create files subdirectories'
  logEventStart "${process}"

  directories="
    _cache
    _cron
    _dumps
    _graphs
    _inventories
    _locales
    _lock
    _log
    _pictures
    _plugins
    _rss
    _sessions
    _tmp
    _uploads
    "
  for directory in ${directories}; do
    if [ ! -d "files/$directory" ]; then
      mkdir "files/$directory"
    fi
  done

  logEventEnd "${process}"
}

clearTmpFiles() {
  process='Clean temporary files'
  logEventStart "${process}"

  find files/_cache -mindepth 1 -maxdepth 1 -exec rm -r {} +
  find files/_tmp -mindepth 1 -maxdepth 1 -exec rm -r {} +

  logEventEnd "${process}"
}

chownFiles() {
  process='Set owner for config and files directories'
  logEventStart "${process}"

  chown www-data: config files -R
  logEventEnd "${process}"
}

prepareInstance() {
  glpi-unsecure-install-directory
  createFilesSubdirectories
  clearTmpFiles
  chownFiles
}

waitDatabase() {
  process='Wait for database is ready'
  logEventStart "${process}"

  php /usr/local/bin/support/wait-database.php &
  wait $!

  logEventEnd "${process}"
}

getDatabaseAutoInstallStatus() {
  process='Check if database is ready for install or update'
  logEventStart "${process}"

  status=0
  php /usr/local/bin/support/check-database.php &
  wait $! || status=$?
  logEventEnd "${process}"

  return $status
}

installDatabase() {
  process='Install database'
  logEventStart "${process}"

  if [ -f config/config_db.php ]; then
    rm config/config_db.php
  fi
  glpi-install

  logEventEnd "${process}"
}

updateDatabase() {
  process='Update database'
  logEventStart "${process}"

  glpi-update

  logEventEnd "${process}"
}

configureCache() {
  process='Configure cache'
  logEventStart "${process}"

  glpi-cache

  logEventEnd "${process}"
}

autoInstall() {
  case "${AUTO_INSTALL}" in
  [YyTt1]*)
    logMessage 'Auto install is enabled'
    ;;
  *)
    logMessage 'Auto install script is disabled'
    return
    ;;
  esac

  waitDatabase
  status=0
  getDatabaseAutoInstallStatus || status=$?
  case "${status}" in
  "1")
    installDatabase
    configureCache
    ;;
  "2")
    updateDatabase
    configureCache
    ;;
  *)
    logMessage 'Database is not ready for auto install'
    return
    ;;
  esac
  glpi-secure-install-directory
}

# shellcheck disable=SC1091
main() {
  . /usr/local/bin/support/environment-variables
  . /usr/local/bin/support/log-events
  . /usr/local/bin/support/traps

  prepareInstance
  autoInstall
}

main
