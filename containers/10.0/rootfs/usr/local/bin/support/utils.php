<?php

declare(strict_types=1);

/**
 * @SuppressWarnings(PHPMD.ExitExpression)
 * @param int $signalNumber
 */
function signalHandler(int $signalNumber): void
{
    switch ($signalNumber) {
        case SIGQUIT:
        case SIGWINCH:
        case SIGTERM:
        case SIGINT:
            /** @noinspection ForgottenDebugOutputInspection */
            error_log("Got signal $signalNumber");
            exit(0);
    }
}

function initSignals(): void
{
    foreach ([SIGTERM, SIGINT, SIGQUIT, SIGWINCH] as $signal) {
        pcntl_signal($signal, 'signalHandler');
    }
}

function logMessage(string $errorMessage, string $prefix = 'Error', ?int $errorCode = null): void
{
    if ($errorCode === null) {
        $errorMessage = sprintf('%s: %s', $prefix, $errorMessage);
    } else {
        $errorMessage = sprintf('%s: (%d) %s', $prefix, $errorCode, $errorMessage);
    }
    /** @noinspection ForgottenDebugOutputInspection */
    error_log($errorMessage);
}

function connectDatabase(): mysqli
{
    $mysqlHostname = getenv('MYSQL_HOSTNAME');
    $mysqlDatabase = getenv('MYSQL_DATABASE');
    $mysqlUser = getenv('MYSQL_USER');
    $mysqlPassword = getenv('MYSQL_PASSWORD');
    return new mysqli($mysqlHostname, $mysqlUser, $mysqlPassword, $mysqlDatabase);
}
