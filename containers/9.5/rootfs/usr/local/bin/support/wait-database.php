<?php

// phpcs:disable PSR1.Files.SideEffects

declare(strict_types=1);

require_once __DIR__ . '/utils.php';

/**
 * @noinspection PhpUnused
 */
function checkDatabase(): int
{
    $mysqlConnection = connectDatabase();
    $errorPrefix = 'Failed to connect to MySQL';
    switch ($mysqlConnection->connect_errno) {
        case 1045:
            logMessage($mysqlConnection->connect_error, $errorPrefix, $mysqlConnection->connect_errno);
            return 1;
        case 0:
            logMessage("Database is available $mysqlConnection->server_info", 'Notice');
            return 0;
        default:
            logMessage($mysqlConnection->connect_error, $errorPrefix, $mysqlConnection->connect_errno);
            return -1;
    }
}

/**
 * @noinspection PhpUnused
 */
function checkSession(): int
{
    $errorPrefix = 'Failed to start session';
    try {
        $status = session_start() && session_destroy();
        logMessage('Session storage is available', 'Notice');
    } catch (Error $error) {
        logMessage($error->getMessage(), $errorPrefix, $error->getCode());
        return -1;
    }
    if ($status === false) {
        logMessage('Can\'t check if session is able to start', $errorPrefix);
        return 1;
    }
    return 0;
}

function main(): int
{
    initSignals();
    $checkResult = -1;
    while ($checkResult === -1) {
        $checks = [
            'checkSession',
            'checkDatabase',
        ];
        $checkResult = array_reduce(
            $checks,
            static function ($acc, $check) {
                $result = $check();
                if ($acc > 0) {
                    return $acc;
                }
                if ($result === -1 || $acc === -1) {
                    return -1;
                }
                return 0;
            },
            0
        );
        sleep(1);
    }
    return $checkResult;
}

/** @SuppressWarnings(PHPMD.ExitExpression) */
exit(main());
